> bubble game

Html game made in CreateJS

## Build Setup

# install dependencies
npm install

# build for production with minification
npm run build

# open dist/index.html in a browser

## How to play

Click on a colored bubble to destroy the connected bubbles with the same color, all the bubbles will go up to fill the empty space.
Click on the rotate button to rearrange the bubbles.

To win the game you must destroy all the bubbles.

To start a new game press the start button
To view, the high scores click on the cup