
var bubbleGame = function (mobile) {
    if (mobile == undefined) {
        this.mobile = false;
    } else {
        this.mobile = mobile;
    }
  
    this.pLeft = this.mobile ? 0 : 190;
    this.pTop = this.mobile ? 0 : 102;
    this.gap = 1;
    this.bRadius = 13;
    this.Colors = ["rozsaszin.png", "kek.png", "narancs.png", "sarga.png", "zold.png"];
    this.dir = 0;
    this.isRotating = false;
    this.score = 0;
    this.gameId = 2441;
    this.bubbles = [];
    this.list = [];
    this.container = {};
    this.bubbleContainer = {};
    this.scoreList = [];
    var t = this;
    
    
    
    this.init = function () {
        
        var ls = localStorage.getItem("scoreList");
        if(ls != null){
            this.scoreList = JSON.parse(ls);
        } 
        var gameCanvas = document.getElementById('gameCanvas');
        gameCanvas.width = 800;
        gameCanvas.height = 696;
        if (this.mobile) {
            gameCanvas.width = 800 - 370;
            gameCanvas.height = 696 - 100;
        }
        this.stage = new createjs.Stage(gameCanvas);
        this.stage.enableMouseOver();
        createjs.Ticker.addEventListener("tick", this.stage);
        this.container = new createjs.Container();
        this.container.x = this.pLeft + 432 / 2;
        this.container.y = this.pTop + 432 / 2;
        var conBg = new createjs.Bitmap("img/lila_hatter1.png");
        this.container.addChild(conBg);
        this.container.regY = this.container.regX = 432 / 2;
        this.bubbleContainer = new createjs.Container();
        this.bubbleContainer.x = this.bubbleContainer.y = 432 / 2;
        this.bubbleContainer.regY = this.bubbleContainer.regX = 15 * (this.bRadius * 2 + this.gap) / 2

        this.container.addChild(this.bubbleContainer);
        this.stage.addChild(this.container);
        this.rotateButton = new createjs.Bitmap("img/nyil.png");
        this.rotateButton.x = this.mobile ? 686 - 310 : 686;
        this.rotateButton.y = 481;
        this.rotateButton.regX = 51;
        this.rotateButton.regY = 51;
        this.rotateButton.cursor = "pointer";
        this.rotateButton.addEventListener("click", this.rotate);
        this.stage.addChild(this.rotateButton);
        var newGameButton = new createjs.Bitmap("img/ujjatekbg.png");
        newGameButton.x = this.mobile ? 0 : 100;
        newGameButton.y = this.mobile ? 340 + 165 : 340;
        newGameButton.cursor = "pointer";
        newGameButton.addEventListener("click", function(){t.newGame()});
        newGameButton.cursor = "pointer";
        this.stage.addChild(newGameButton);
        var ngLabel = new createjs.Bitmap("img/ujjatek.png");
        ngLabel.x = this.mobile ? 17 : 117;
        ngLabel.y = 417 + (this.mobile ? 165 : 0);
        ngLabel.alpha = 0;
        this.stage.addChild(ngLabel);
        newGameButton.addEventListener('mouseover', function () {
            createjs.Tween.get(ngLabel).to({alpha: 1}, 200);
        });
        newGameButton.addEventListener('mouseout', function () {
            createjs.Tween.get(ngLabel).to({alpha: 0}, 200);
        });
        var topButton = new createjs.Bitmap("img/legjobbak.png");
        //newGameButton.graphics.beginFill("black").drawCircle(100, 600, 40);
        topButton.x = this.mobile ? 0 : 100;
        topButton.y = this.mobile ? 245 + 180 : 245;
        topButton.cursor = "pointer";
        topButton.addEventListener("click", function(e){e.preventDefault();e.stopPropagation();t.getHighScores(); });
        topButton.cursor = "pointer";
        this.stage.addChild(topButton);
        var topLabel = new createjs.Bitmap("img/legjobbakfel.png");
        topLabel.x = this.mobile ? 8 : 108;
        topLabel.y = 320 + (this.mobile ? 180 : 0);
        topLabel.alpha = 0;
        this.stage.addChild(topLabel);
        topButton.addEventListener('mouseover', function () {
            createjs.Tween.get(topLabel).to({alpha: 1}, 200);
        });
        topButton.addEventListener('mouseout', function () {
            createjs.Tween.get(topLabel).to({alpha: 0}, 200);
        });
        this.scoreBg = new createjs.Bitmap("img/pontbg.png");
        this.scoreBg.x = 100 + (this.mobile ? 75 : 0);
        this.scoreBg.y = this.mobile ? 505 : 435;
        this.stage.addChild(this.scoreBg);
        this.scoreText = new createjs.Text('this.score');
        this.scoreText.text = "";
        this.scoreText.x = 140 + (this.mobile ? 75 : 0);
        this.scoreText.y = 459 + (this.mobile ? 70 : 0);
        this.scoreText.font = "bold 26px Arial";
        this.scoreText.color = '#e9eee4';
        this.scoreText.textAlign = 'center';
        this.stage.addChild(this.scoreText);

        this.createTopList();
        this.newGame();
    }


    this.getHighScores = function () {
        
       
        this.showList();
        
    }
    this.sendScore = function (score) {
        var name = window.prompt("Your name:");
        this.scoreList.push([name,score]);
        this.scoreList.sort(function(a,b){
            if(a[1]>b[1]){
                return -1;
            };
            if(a[1]<b[1]){
                return 1;
            };
            return 0;
        });
        localStorage.setItem("scoreList",JSON.stringify(this.scoreList));
        
    }
    


    this.getNeighbours = function (b) {
        var result = [];
        if (this.bubbles[b.bx - 1] != null && this.bubbles[b.bx - 1][b.by] != null)
            result.push(this.bubbles[b.bx - 1][b.by]);
        if (this.bubbles[b.bx + 1] != null && this.bubbles[b.bx + 1][b.by] != null)
            result.push(this.bubbles[b.bx + 1][b.by]);
        if (this.bubbles[b.bx][b.by + 1] != null)
            result.push(this.bubbles[b.bx][b.by + 1]);
        if (this.bubbles[b.bx][b.by - 1] != null)
            result.push(this.bubbles[b.bx][b.by - 1]);
        
        return result;
    }

    this.bubbleClick = function (e) {
        if (t.isRotating)
            return;
        var bubble = e.target;
        var n = t.getNeighbours(bubble);
        
        //check colors
        var sameColor = false;
        for (var i = 0; i < n.length; i++) {
            if (bubble.bc == n[i].bc) {
                sameColor = true;
            }
        }
        if (sameColor) {
            t.getSameColor(bubble);
        }

    }
    this.deleteBubble = function (b) {
        this.bubbleContainer.removeChild(b);
        var x = b.bx;
        var y = b.by + 1;
        this.bubbles[b.bx][b.by] = null;
    }
    this.getSameColor = function (b) {
        var n = this.getNeighbours(b);
        var nn;
        var sc = [b];
        while (n != 0) {
            var current = n.pop();
            if (current.bc == b.bc) {
                sc.push(current);
                nn = this.getNeighbours(current);
                for (var i = 0; i < nn.length; i++) {
                    if (sc.indexOf(nn[i]) === -1) {
                        n.push(nn[i]);
                    }
                }

            }
        }
        
        for (var i = 0; i < sc.length; i++) {
            this.deleteBubble(sc[i]);
        }
        if (sc.length > 3) {
            this.setScore(this.score + sc.length * sc.length);
        }
        this.bubbleMoveUp();
    }

    this.bubbleMoveUp = function () {
        if (this.bubbleContainer.children.length == 0) {
            alert('You have won!');
            this.sendScore(this.score);
            this.newGame();
            return;
        }
   
        
        if (this.dir == 0) {
            for (var i = 0; i < 15; i++) {
                for (var j = 0; j < 15; j++) {
                    if (j != 0 && this.bubbles[i][j - 1] == null && this.bubbles[i][j] != null) {
                        var ny = j - 1;
                        while (this.bubbles[i][ny - 1] == null && ny > 0) {
                            ny--;
                        }
                        this.bubbles[i][ny] = this.bubbles[i][j];
                        this.bubbles[i][j] = null;
                        this.bubbles[i][ny].moveY(ny);
                    }
                }
            }
        }

        if (this.dir == 1) {
            for (var i = 0; i < 15; i++) {
                for (var j = 0; j < 15; j++) {
                    if (j != 0 && this.bubbles[j - 1][i] == null && this.bubbles[j][i] != null) {
                        var ny = j - 1;
                        while (ny > 0 && this.bubbles[ny - 1][i] == null) {
                            ny--;
                        }
                        this.bubbles[ny][i] = this.bubbles[j][i];
                        this.bubbles[j][i] = null;
                        this.bubbles[ny][i].moveX(ny);
                    }
                }
            }
        }
        if (this.dir == 2) {
            for (var i = 0; i < 15; i++) {
                for (var j = 14; j >= 0; j--) {
                    if (j != 14 && this.bubbles[i][j + 1] == null && this.bubbles[i][j] != null) {
                        var ny = j + 1;
                        while (this.bubbles[i][ny + 1] == null && ny < 14) {
                            ny++;
                        }
                        this.bubbles[i][ny] = this.bubbles[i][j];
                        this.bubbles[i][j] = null;
                        this.bubbles[i][ny].moveY(ny);
                    }
                }
            }
        }
        if (this.dir == 3) {
            for (var i = 0; i < 15; i++) {
                for (var j = 14; j >= 0; j--) {
                    if (j != 14 && this.bubbles[j + 1][i] == null && this.bubbles[j][i] != null) {
                        var ny = j + 1;
                        while (ny < 14 && this.bubbles[ny + 1][i] == null) {
                            ny++;
                        }
                        this.bubbles[ny][i] = this.bubbles[j][i];
                        this.bubbles[j][i] = null;
                        this.bubbles[ny][i].moveX(ny);
                    }
                }
            }
        }
    }

    this.rotate = function () {
        t.dir--;
        var targetDeg = t.container.rotation - 90;
        t.rotateButton.removeEventListener("click", t.rotate);
        t.isRotating = true;
        createjs.Tween.get(t.container).to({rotation: targetDeg}, 500).call(function(){t.endRotation()});
        for (var o of t.bubbleContainer.children) {
            createjs.Tween.get(o).to({rotation: -targetDeg}, 500);
        }
        createjs.Tween.get(t.rotateButton).to({rotation: -360}, 500, createjs.Ease.cubicOut).call(function () {
            t.rotateButton.rotation = 0
        });
        if (t.dir < 0) {
            t.dir = 3;
           
        }
    }

    this.endRotation = function () {
        this.isRotating = false;
        this.bubbleMoveUp();
        this.rotateButton.addEventListener("click", this.rotate);
    }

    this.newGame = function () {
        this.setScore(0);
        this.container.rotation = 0;
        this.dir = 0;
        this.bubbleContainer.removeAllChildren();
        for (var i = 0; i < 15; i++) {
            this.bubbles[i] = [];
            for (var j = 0; j < 15; j++) {
                var color = Math.floor(Math.random() * 3);
                var bubble = new createjs.Bitmap("img/" + this.Colors[color]);
                bubble.cursor = "pointer";
                this.bubbleContainer.addChild(bubble);
                bubble.x = i * (this.bRadius * 2 + this.gap) + this.bRadius;
                bubble.y = j * (this.bRadius * 2 + this.gap) + this.bRadius;
                bubble.regX = bubble.regY = this.bRadius;
                bubble.bx = i;
                bubble.by = j;
                bubble.moveY = function (newy) {
                    var diff = Math.abs(newy - this.by);
                    this.by = newy;
                    createjs.Tween.get(this).to({y: newy * (t.gap + 2 * t.bRadius) + t.bRadius}, Math.pow(diff * 50000, 0.5), createjs.Ease.getPowIn(2));
                }
                bubble.moveX = function (newx) {
                    var diff = Math.abs(newx - this.bx);
                    this.bx = newx;
                    createjs.Tween.get(this).to({x: newx * (t.gap + 2 * t.bRadius) + t.bRadius}, Math.pow(diff * 50000, 0.5), createjs.Ease.getPowIn(2));
                }
                bubble.bc = color;
                bubble.addEventListener("click", this.bubbleClick);
                this.bubbles[i][j] = bubble;
            }
        }
    }


    this.setScore = function (s) {
        this.score = s;
        this.scoreText.text = this.score;
    }




    this.createTopList = function () {
       
        this.list = new createjs.Container()
        this.list.x = 0;
        this.list.y = 0;
        var transparent = new createjs.Shape();
        transparent.graphics.beginFill('#fff').drawRect(0, 0, gameCanvas.width, gameCanvas.height);
        transparent.alpha = 0.1;
        this.list.addChild(transparent);
        var innerList = new createjs.Container();
        this.list.addChild(innerList);
        innerList.x = this.pLeft + 25;
        innerList.y = this.pTop + 25;
        var back = new createjs.Shape();
        innerList.addChild(back);
        back.x = 0;
        back.y = 0;
        back.graphics.beginFill('#91e1f9').setStrokeStyle(10).beginStroke('#58b0ee').drawRect(0, 0, 380, 380);
        var llLabel = new createjs.Text('High score');
        llLabel.x = 190;
        llLabel.y = 20;
        llLabel.font = "bold 26px Arial";
        llLabel.color = '#000';
        llLabel.textAlign = 'center';
        innerList.addChild(llLabel);
        this.peoples = [];
        this.scores = [];
        for (var i = 0; i < 10; i++) {
            this.peoples[i] = new createjs.Text('test nevek');
            this.peoples[i].x = 30;
            this.scores[i] = new createjs.Text('8888');
            this.scores[i].x = 360;
            this.scores[i].textAlign = 'right';
            this.peoples[i].y = this.scores[i].y = 70 + i * 27;
            this.peoples[i].font = this.scores[i].font = "bold 16px Arial";
            innerList.addChild(this.scores[i]);
            innerList.addChild(this.peoples[i]);
        }

        this.list.alpha = 0;
        this.stage.addChild(this.list);
    }

    this.showList = function () {
        console.log(this);
        for (var i = 0; i < 10; i++) {
            this.peoples[i].text = this.scoreList.length > i ? this.scoreList[i][0] : '';
            this.scores[i].text = this.scoreList.length > i ? this.scoreList[i][1] : '';
        }
        this.list.alpha = 1;
        this.stage.cursor = 'pointer';
        this.stage.addEventListener('click', function (e) {
            
            e.stopImmediatePropagation();
            t.stage.removeEventListener('click');
            t.list.alpha = 0;
            t.stage.cursor = '';
        });
    }
    
    this.init();


}

export default bubbleGame;


