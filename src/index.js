import createjs from "createjs";
import bubbleGame from './bubbleGame'

        function resize() {
            document.getElementById('gameContainer').style.height = window.innerHeight + "px";
        }


window.onload = function () {
    var w = window.innerWidth;
    var h = window.innerHeight;
    var mobile = false;
    if (w < 500)
        mobile = true;
    var ratio = mobile ? ((800 - 370) / (696 - 100)) : (800 / 696);
    if (w / h > ratio) {
        document.getElementById('gameContainer').style['max-width'] = (h * ratio * 0.95) + "px";
    }
    window.game = new bubbleGame(mobile);
    window.onresize = resize;
    resize();
};